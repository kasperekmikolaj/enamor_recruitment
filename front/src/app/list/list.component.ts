import {Component, OnInit} from '@angular/core';
import {TemperatureService} from "../services/temperature-service";
import {CountryWithCitesAvgsModel} from "../models/country-with-cites-avgs.model";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  countryWithCitiesAvgs: CountryWithCitesAvgsModel;
  isDataReady: boolean = false;

  constructor(private temperatureService: TemperatureService) {}

  ngOnInit(): void {
    this.temperatureService.getAvgs().subscribe((data: CountryWithCitesAvgsModel) => {
      this.countryWithCitiesAvgs = data;
      this.countryWithCitiesAvgs.citiesAvgList.sort((a, b) => a.city > b.city ? 1 : -1);
      console.log(this.countryWithCitiesAvgs)
      this.isDataReady = true;
    });
  }

}
