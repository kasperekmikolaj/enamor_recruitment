import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Constants} from "../constants";
import {CreateTemperatureRecordDto} from "../models/dto/create-temperature-record.dto";
import {Observable} from "rxjs";
import {CountryWithCitesAvgsModel} from "../models/country-with-cites-avgs.model";

@Injectable({
  providedIn: 'root'
})
export class TemperatureService {

  constructor(private httpClient: HttpClient) {}

  public addNewRecord(createDto: CreateTemperatureRecordDto): void {
    this.httpClient.post(Constants.BACKEND + "temperature", createDto).subscribe();
  }

  public getAvgs(): Observable<CountryWithCitesAvgsModel>{
    return this.httpClient.get<CountryWithCitesAvgsModel>(Constants.BACKEND + "temperature")
  }

}
