export class CountryAvgForDayModel {

  constructor(public day: Date,
              public avgTemperature: number) {}
}
