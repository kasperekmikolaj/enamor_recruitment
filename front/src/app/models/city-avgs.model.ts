export class CityAvgsModel {

  constructor(public city: string,
              public daytimeAvg: number,
              public nightAvg: number,
              public fullDayAvg: number,
              public day: Date) {}
}
