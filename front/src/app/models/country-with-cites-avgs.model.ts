import {CityAvgsModel} from "./city-avgs.model";
import {CountryAvgForDayModel} from "./country-avg-for-day.model";

export class CountryWithCitesAvgsModel {

  constructor(
    public citiesAvgList: CityAvgsModel[],
    public countryAvgByDateList: CountryAvgForDayModel[],) {}
}
