export class CreateTemperatureRecordDto {

  constructor(
    public cityName: string,
    public temperature: number,) {}
}
