import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validator, Validators} from "@angular/forms";
import {Constants} from "../constants";
import {Router} from "@angular/router";
import {CreateTemperatureRecordDto} from "../models/dto/create-temperature-record.dto";
import {TemperatureService} from "../services/temperature-service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  addRecordFormGroup: FormGroup;
  cityNameForm: FormControl;
  temperatureForm: FormControl;

  constructor(private router: Router, private temperatureService: TemperatureService) {
    this.cityNameForm = new FormControl('',
      [Validators.minLength(Constants.MIN_CITY_NAME_LEN), Validators.required,
        Validators.maxLength(Constants.MAX_CITY_NAME_LEN)]);
    this.temperatureForm = new FormControl('', [Validators.required, Validators.min(Constants.MIN_ALLOWED_TEMPERATURE),
      Validators.max(Constants.MAX_ALLOWED_TEMPERATURE)]);
    this.addRecordFormGroup = new FormGroup({
      cityNameInput: this.cityNameForm,
      temperatureInput: this.temperatureForm,
    });
  }

  ngOnInit(): void {}


  onSubmit() {
    const cityName = this.cityNameForm.value;
    const temperature = this.temperatureForm.value;
    const createRecordDto = new CreateTemperatureRecordDto(cityName, temperature);
    this.temperatureService.addNewRecord(createRecordDto);
    this.addRecordFormGroup.reset();
  }

  goToList() {
    this.router.navigate(['/list']);
  }
}
