export class Constants {
  public static MIN_CITY_NAME_LEN = 2;
  public static MAX_CITY_NAME_LEN  = 100;
  public static MIN_ALLOWED_TEMPERATURE = -273;
  public static MAX_ALLOWED_TEMPERATURE = 120;

  public static BACKEND = "http://localhost:8080/";
}
