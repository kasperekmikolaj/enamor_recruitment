package com.example.ena_back.service;

import com.example.ena_back.dto.CountryWithCitiesDto;
import com.example.ena_back.dto.create.CreateTemperatureRecordDto;
import com.example.ena_back.exceptions.TemperatureCalcException;

public interface TemperatureService {

    void addNewTemperatureRecordForCity(CreateTemperatureRecordDto createTemperatureRecordDto);

    CountryWithCitiesDto getCountryAndCitiesAvgTemperatures() throws TemperatureCalcException;

}
