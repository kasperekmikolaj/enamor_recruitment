package com.example.ena_back.service.impl;

import com.example.ena_back.consts.DayTimeConstant;
import com.example.ena_back.dto.CityAvgTemperaturesDto;
import com.example.ena_back.dto.CountryAvgForDayDto;
import com.example.ena_back.dto.CountryWithCitiesDto;
import com.example.ena_back.dto.create.CreateTemperatureRecordDto;
import com.example.ena_back.exceptions.TemperatureCalcException;
import com.example.ena_back.persistence.TemperatureRecord;
import com.example.ena_back.service.TemperatureService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@Scope("application")
public class TemperatureServiceImpl implements TemperatureService {

    private final Map<String, List<TemperatureRecord>> temperaturesHistory = new HashMap<>();

    // some testing data
    TemperatureServiceImpl() {
        String cityName1 = "Warszawa";
        String cityName2 = "Kraków";

        List<TemperatureRecord> list1 = new ArrayList<TemperatureRecord>();
        List<TemperatureRecord> list2 = new ArrayList<TemperatureRecord>();

        TemperatureRecord record1 = new TemperatureRecord(10, LocalDate.of(2022, 7, 5), LocalTime.of(5,44));
        TemperatureRecord record2 = new TemperatureRecord(12, LocalDate.of(2022, 7, 4), LocalTime.of(5,55));
        TemperatureRecord record3 = new TemperatureRecord(7, LocalDate.of(2022, 7, 5), LocalTime.of(5,23));
        TemperatureRecord record4 = new TemperatureRecord(21, LocalDate.of(2022, 7, 4), LocalTime.of(8,24));
        TemperatureRecord record5 = new TemperatureRecord(29, LocalDate.of(2022, 7, 5), LocalTime.of(9,25));
        TemperatureRecord record6 = new TemperatureRecord(46, LocalDate.of(2022, 7, 4), LocalTime.of(10,26));

        TemperatureRecord record7 = new TemperatureRecord(46, LocalDate.of(2022, 7, 5), LocalTime.of(9,26));
        TemperatureRecord record8 = new TemperatureRecord(46, LocalDate.of(2022, 7, 4), LocalTime.of(7,26));
        TemperatureRecord record9 = new TemperatureRecord(46, LocalDate.of(2022, 7, 5), LocalTime.of(9,26));
        TemperatureRecord record10 = new TemperatureRecord(46, LocalDate.of(2022, 7, 4), LocalTime.of(4,26));
        TemperatureRecord record11 = new TemperatureRecord(46, LocalDate.of(2022, 7, 5), LocalTime.of(2,26));
        TemperatureRecord record12 = new TemperatureRecord(46, LocalDate.of(2022, 7, 4), LocalTime.of(3,26));

        list1.add(record1);
        list1.add(record2);
        list1.add(record3);
        list2.add(record4);
        list2.add(record5);
        list2.add(record6);

        list1.add(record7);
        list1.add(record8);
        list1.add(record9);
        list2.add(record10);
        list2.add(record11);
        list2.add(record12);

        this.temperaturesHistory.put(cityName1, list1);
        this.temperaturesHistory.put(cityName2, list2);
    }

    @Override
    public void addNewTemperatureRecordForCity(CreateTemperatureRecordDto createTemperatureRecordDto) {
        String cityNameLowerCase = createTemperatureRecordDto.getCityName().toLowerCase();
        if (this.temperaturesHistory.containsKey(cityNameLowerCase)) {
            this.temperaturesHistory
                    .get(cityNameLowerCase)
                    .add(new TemperatureRecord(createTemperatureRecordDto.getTemperature(), LocalDate.now(), LocalTime.now()));
        } else { // new city
            ArrayList<TemperatureRecord> newCityList = new ArrayList<>();
            newCityList.add(new TemperatureRecord(createTemperatureRecordDto.getTemperature(), LocalDate.now(), LocalTime.now()));
            this.temperaturesHistory.put(cityNameLowerCase, newCityList);
        }
    }

    @Override
    public CountryWithCitiesDto getCountryAndCitiesAvgTemperatures() throws TemperatureCalcException {
        ArrayList<CityAvgTemperaturesDto> citiesAvgList = new ArrayList<>();
        for (Map.Entry<String, List<TemperatureRecord>> entry : this.temperaturesHistory.entrySet()) {
            String cityNameBigLetter = makeCityNameFirstLettersBig(entry.getKey());
            // get dates for single city
            Set<LocalDate> recordedDatesForCity = this.temperaturesHistory.get(entry.getKey())
                    .stream()
                    .map(TemperatureRecord::getDate)
                    .collect(Collectors.toSet());
            for (LocalDate day : recordedDatesForCity) {
                // get dates from which we got records
                // calc avg
                int daytimeAvg = entry.getValue()
                        .stream()
                        .filter(elem -> day.equals(elem.getDate()))
                        .filter(elem -> elem.getTime().isAfter(DayTimeConstant.DAY_START_LOCAL_TIME) &&
                                elem.getTime().isBefore(DayTimeConstant.DAY_END_LOCAL_TIME) ||
                                elem.getTime().equals(DayTimeConstant.DAY_START_LOCAL_TIME))
                        .mapToInt(TemperatureRecord::getTemperature)
                        .average()
                        .stream()
                        .map(Math::round)
                        .mapToInt(result -> (int) result)
                        .findFirst()
                        // no daytime values
                        .orElse(0);

                int nightAvg = entry.getValue()
                        .stream()
                        .filter(elem -> day.equals(elem.getDate()))
                        .filter(elem -> elem.getTime().isAfter(DayTimeConstant.DAY_END_LOCAL_TIME) ||
                                elem.getTime().isBefore(DayTimeConstant.DAY_START_LOCAL_TIME) ||
                                elem.getTime().equals(DayTimeConstant.DAY_END_LOCAL_TIME))
                        .mapToInt(TemperatureRecord::getTemperature)
                        .average()
                        .stream()
                        .map(Math::round)
                        .mapToInt(result -> (int) result)
                        .findFirst()
                        // no nighttime values
                        .orElse(0);

                int fullDayAvg = entry.getValue()
                        .stream()
                        .filter(elem -> day.equals(elem.getDate()))
                        .mapToInt(TemperatureRecord::getTemperature)
                        .average()
                        .stream()
                        .map(Math::round)
                        .mapToInt(result -> (int) result)
                        .findFirst()
                        .orElseThrow(TemperatureCalcException::new);

                citiesAvgList.add(new CityAvgTemperaturesDto(cityNameBigLetter, daytimeAvg, nightAvg, fullDayAvg, day));
            }
        }
        // calc country avg
        Set<LocalDate> datesSet = citiesAvgList
                .stream()
                .map(CityAvgTemperaturesDto::getDay)
                .collect(Collectors.toSet());

        // no place to enter country required so I suppose that there is one county ;p
        List<CountryAvgForDayDto> countryAvgByDate = new ArrayList<>();
        for (LocalDate day : datesSet) {
            int countryAvg = citiesAvgList
                    .stream()
                    .filter(elem -> elem.getDay().isEqual(day))
                    .mapToInt(CityAvgTemperaturesDto::getFullDayAvg)
                    .average()
                    .stream()
                    .map(Math::round)
                    .mapToInt(result -> (int) result)
                    .findFirst()
                    .orElseThrow(TemperatureCalcException::new);
            countryAvgByDate.add(new CountryAvgForDayDto(day, countryAvg));
        }
        return new CountryWithCitiesDto(citiesAvgList, countryAvgByDate);
    }

    //    https://www.programiz.com/java-programming/examples/capitalize-first-character-of-string
    private String makeCityNameFirstLettersBig(String cityName) {
        // stores each characters to a char array
        char[] charArray = cityName.toCharArray();
        boolean foundSpace = true;
        for (int i = 0; i < charArray.length; i++) {
            // if the array element is a letter
            if (Character.isLetter(charArray[i])) {
                // check space is present before the letter
                if (foundSpace) {
                    // change the letter into uppercase
                    charArray[i] = Character.toUpperCase(charArray[i]);
                    foundSpace = false;
                }
            } else {
                // if the new character is not character
                foundSpace = true;
            }
        }
        // convert the char array to the string
        String res = String.valueOf(charArray);
        return res;
    }

}
