package com.example.ena_back.dto.create;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
public class CreateTemperatureRecordDto {

    @NotBlank
    @Size(min = 2, max = 100)
    private String cityName;

    @NotNull
    @Min(-273)
    @Max(120)
    private Integer temperature;
}
