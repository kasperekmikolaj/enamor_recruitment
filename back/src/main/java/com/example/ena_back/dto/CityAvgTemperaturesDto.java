package com.example.ena_back.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CityAvgTemperaturesDto {

    private String city;
    private Integer daytimeAvg;
    private Integer nightAvg;
    private Integer fullDayAvg;
    private LocalDate day;
}
