package com.example.ena_back.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CountryWithCitiesDto {

    private List<CityAvgTemperaturesDto> citiesAvgList;
    private List<CountryAvgForDayDto> countryAvgByDateList;

}
