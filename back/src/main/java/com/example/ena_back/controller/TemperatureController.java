package com.example.ena_back.controller;

import com.example.ena_back.dto.CountryWithCitiesDto;
import com.example.ena_back.dto.create.CreateTemperatureRecordDto;
import com.example.ena_back.service.TemperatureService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("temperature")
@AllArgsConstructor
public class TemperatureController {

    private final TemperatureService temperatureService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addNewTemperatureRecord(@Valid @RequestBody CreateTemperatureRecordDto createTemperatureRecordDto) {
        this.temperatureService.addNewTemperatureRecordForCity(createTemperatureRecordDto);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CountryWithCitiesDto getCountryWithCitiesAvg() {
        return this.temperatureService.getCountryAndCitiesAvgTemperatures();
    }

}
