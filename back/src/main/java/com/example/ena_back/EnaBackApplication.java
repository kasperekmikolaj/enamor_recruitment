package com.example.ena_back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class EnaBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnaBackApplication.class, args);
    }

}
