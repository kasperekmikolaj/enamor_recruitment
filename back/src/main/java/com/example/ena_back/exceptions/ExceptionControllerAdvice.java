package com.example.ena_back.exceptions;

import com.example.ena_back.dto.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(TemperatureCalcException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handleTemperatureCalcException(TemperatureCalcException exception) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("Life is brutal and math is hard");
        return errorDto;
    }

}
