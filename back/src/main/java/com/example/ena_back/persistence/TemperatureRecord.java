package com.example.ena_back.persistence;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TemperatureRecord {

    @NotNull
    @Min(-273)
    @Max(120)
    private Integer temperature;
    @NotNull
    private LocalDate date;
    @NotNull
    private LocalTime time;
}
