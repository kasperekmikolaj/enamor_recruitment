package com.example.ena_back.consts;

import org.springframework.context.annotation.Configuration;

import java.time.LocalTime;

@Configuration
public class DayTimeConstant {

    public static final LocalTime DAY_START_LOCAL_TIME = LocalTime.of(6,0,0,0);
    public static final LocalTime DAY_END_LOCAL_TIME = LocalTime.of(20,0,0,0);

}
