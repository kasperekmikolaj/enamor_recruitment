package com.example.ena_back.service;

import com.example.ena_back.dto.CountryWithCitiesDto;
import com.example.ena_back.dto.create.CreateTemperatureRecordDto;
import com.example.ena_back.service.impl.TemperatureServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TemperatureServiceImplTest {

    @Autowired
    private TemperatureServiceImpl cityTemperatureService;

    @Test
    public void cityNameShouldStartWithBigLetter() {
        // given
        CreateTemperatureRecordDto recordDto1 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto2 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto3 = new CreateTemperatureRecordDto();

        recordDto1.setCityName("new YOrk");
        recordDto2.setCityName("new yoRK");
        recordDto3.setCityName("NEW york");

        recordDto1.setTemperature(1);
        recordDto2.setTemperature(3);
        recordDto3.setTemperature(5);

        // when
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto1);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto2);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto3);

        // then
        CountryWithCitiesDto res = this.cityTemperatureService.getCountryAndCitiesAvgTemperatures();
        assertEquals("New York",
                res.getCitiesAvgList().get(0).getCity());
    }

    @Test
    public void shouldGetTwoCitiesAvg() {
        // given
        CreateTemperatureRecordDto recordDto1 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto2 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto3 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto4 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto5 = new CreateTemperatureRecordDto();

        recordDto1.setCityName("new YOrk");
        recordDto2.setCityName("new yoRK");
        recordDto3.setCityName("NEW york");
        recordDto4.setCityName("warszawa");
        recordDto5.setCityName("warSZAwa");

        recordDto1.setTemperature(1);
        recordDto2.setTemperature(3);
        recordDto3.setTemperature(5);
        recordDto4.setTemperature(20);
        recordDto5.setTemperature(22);

        // when
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto1);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto2);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto3);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto4);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto5);

        // then
        CountryWithCitiesDto res = this.cityTemperatureService.getCountryAndCitiesAvgTemperatures();
        assertEquals(2,
                res.getCitiesAvgList().size());
    }

    @Test
    public void shouldRoundDownCountryAvgTempForCurrentDate() {
        // given
        CreateTemperatureRecordDto recordDto1 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto2 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto3 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto4 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto5 = new CreateTemperatureRecordDto();

        recordDto1.setCityName("new YOrk");
        recordDto2.setCityName("new yoRK");
        recordDto3.setCityName("NEW york");
        recordDto4.setCityName("warszawa");
        recordDto5.setCityName("warSZAwa");

        recordDto1.setTemperature(1);
        recordDto2.setTemperature(3);
        recordDto3.setTemperature(6);
        recordDto4.setTemperature(20);
        recordDto5.setTemperature(22);

        // when
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto1);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto2);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto3);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto4);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto5);

        // then
        CountryWithCitiesDto res = this.cityTemperatureService.getCountryAndCitiesAvgTemperatures();
        assertEquals(12,
                res.getCountryAvgByDateList().get(LocalDate.now()));
    }

    @Test
    public void shouldRoundUpCountryAvgTempForCurrentDate() {
        // given
        CreateTemperatureRecordDto recordDto1 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto2 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto3 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto4 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto5 = new CreateTemperatureRecordDto();

        recordDto1.setCityName("new YOrk");
        recordDto2.setCityName("new yoRK");
        recordDto3.setCityName("NEW york");
        recordDto4.setCityName("warszawa");
        recordDto5.setCityName("warSZAwa");

        recordDto1.setTemperature(1);
        recordDto2.setTemperature(3);
        recordDto3.setTemperature(7);
        recordDto4.setTemperature(20);
        recordDto5.setTemperature(22);

        // when
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto1);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto2);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto3);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto4);
        this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto5);

        // then
        CountryWithCitiesDto res = this.cityTemperatureService.getCountryAndCitiesAvgTemperatures();
        assertEquals(13,
                res.getCountryAvgByDateList().get(LocalDate.now()));
    }

    @Test
    public void shouldCreateCountryAvgForTwoDays() {
        // given
        LocalDate date1 = LocalDate.of(2022, 11, 12);
        LocalDate date2 = LocalDate.of(2022, 11, 13);

        CreateTemperatureRecordDto recordDto1 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto2 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto3 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto4 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto5 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto6 = new CreateTemperatureRecordDto();

        recordDto1.setCityName("new YOrk");
        recordDto2.setCityName("new yoRK");
        recordDto3.setCityName("NEW york");
        recordDto4.setCityName("warszawa");
        recordDto5.setCityName("warszawa");
        recordDto6.setCityName("warszaWA");

        recordDto1.setTemperature(1);
        recordDto2.setTemperature(3);
        recordDto3.setTemperature(5);
        recordDto4.setTemperature(10);
        recordDto5.setTemperature(30);
        recordDto6.setTemperature(50);

        // when
        try (MockedStatic<LocalDate> localDateMockedStatic = mockStatic(LocalDate.class)) {
            when(LocalDate.now()).thenReturn(date1, date1, date2, date2, date2);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto1);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto2);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto3);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto4);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto5);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto6);
        }

        // then
        CountryWithCitiesDto res = this.cityTemperatureService.getCountryAndCitiesAvgTemperatures();
        assertEquals(2, res.getCountryAvgByDateList().size());
    }

    @Test
    public void avgCalculationForSingleCityTest() {
        // given
        //night
        LocalTime time1 = LocalTime.of(20, 0);
        LocalTime time2 = LocalTime.of(5, 59);
        LocalTime time3 = LocalTime.of(4, 12);

        //day
        LocalTime time4 = LocalTime.of(6, 0);
        LocalTime time5 = LocalTime.of(19, 59);
        LocalTime time6 = LocalTime.of(12, 12);

        CreateTemperatureRecordDto recordDto1 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto2 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto3 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto4 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto5 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto6 = new CreateTemperatureRecordDto();

        recordDto1.setCityName("new YOrk");
        recordDto2.setCityName("new yoRK");
        recordDto3.setCityName("NEW york");
        recordDto4.setCityName("NEW york");
        recordDto5.setCityName("NEW york");
        recordDto6.setCityName("NEW york");

        recordDto1.setTemperature(1);
        recordDto2.setTemperature(3);
        recordDto3.setTemperature(5);

        recordDto4.setTemperature(10);
        recordDto5.setTemperature(30);
        recordDto6.setTemperature(50);

        // when
        try (MockedStatic<LocalTime> localTimeMockedStatic = mockStatic(LocalTime.class)) {
            when(LocalTime.now()).thenReturn(time1, time2, time3, time4, time5, time6);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto1);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto2);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto3);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto4);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto5);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto6);
        }

        // then
        CountryWithCitiesDto res = this.cityTemperatureService.getCountryAndCitiesAvgTemperatures();
        assertEquals(1, res.getCitiesAvgList().size());
        assertEquals(30, res.getCitiesAvgList().get(0).getDaytimeAvg());
        assertEquals(3, res.getCitiesAvgList().get(0).getNightAvg());
        assertEquals(17, res.getCitiesAvgList().get(0).getFullDayAvg());
    }

    @Test
    public void nightTempAvgShouldBeZero() {
        // given
        //day
        LocalTime time1 = LocalTime.of(18, 0);
        LocalTime time2 = LocalTime.of(7, 59);
        LocalTime time3 = LocalTime.of(8, 12);
        LocalTime time4 = LocalTime.of(9, 0);
        LocalTime time5 = LocalTime.of(19, 59);
        LocalTime time6 = LocalTime.of(12, 12);
        // night - none

        CreateTemperatureRecordDto recordDto1 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto2 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto3 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto4 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto5 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto6 = new CreateTemperatureRecordDto();

        recordDto1.setCityName("new YOrk");
        recordDto2.setCityName("new yoRK");
        recordDto3.setCityName("NEW york");
        recordDto4.setCityName("NEW york");
        recordDto5.setCityName("NEW york");
        recordDto6.setCityName("NEW york");

        recordDto1.setTemperature(1);
        recordDto2.setTemperature(3);
        recordDto3.setTemperature(5);
        recordDto4.setTemperature(10);
        recordDto5.setTemperature(30);
        recordDto6.setTemperature(50);

        // when
        try (MockedStatic<LocalTime> localTimeMockedStatic = mockStatic(LocalTime.class)) {
            localTimeMockedStatic.when(LocalTime::now).thenReturn(time1, time2, time3, time4, time5, time6);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto1);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto2);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto3);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto4);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto5);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto6);
        }


        // then
        CountryWithCitiesDto res = this.cityTemperatureService.getCountryAndCitiesAvgTemperatures();
        assertEquals(1, res.getCitiesAvgList().size());
        assertEquals(17, res.getCitiesAvgList().get(0).getDaytimeAvg());
        assertEquals(0, res.getCitiesAvgList().get(0).getNightAvg());
        assertEquals(17, res.getCitiesAvgList().get(0).getFullDayAvg());
    }

    @Test
    public void daytimeTempAvgShouldBeZero() {
        // given
        // night
        LocalTime time1 = LocalTime.of(22, 0);
        LocalTime time2 = LocalTime.of(0, 0);
        LocalTime time3 = LocalTime.of(1, 12);
        LocalTime time4 = LocalTime.of(2, 0);
        LocalTime time5 = LocalTime.of(3, 59);
        LocalTime time6 = LocalTime.of(5, 59);
        //day - none

        CreateTemperatureRecordDto recordDto1 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto2 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto3 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto4 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto5 = new CreateTemperatureRecordDto();
        CreateTemperatureRecordDto recordDto6 = new CreateTemperatureRecordDto();

        recordDto1.setCityName("new YOrk");
        recordDto2.setCityName("new yoRK");
        recordDto3.setCityName("NEW york");
        recordDto4.setCityName("NEW york");
        recordDto5.setCityName("NEW york");
        recordDto6.setCityName("NEW york");

        recordDto1.setTemperature(1);
        recordDto2.setTemperature(3);
        recordDto3.setTemperature(5);
        recordDto4.setTemperature(10);
        recordDto5.setTemperature(30);
        recordDto6.setTemperature(50);

        // when
        try (MockedStatic<LocalTime> mockedStatic = mockStatic(LocalTime.class)) {
            mockedStatic.when(LocalTime::now).thenReturn(time1, time2, time3, time4, time5, time6);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto1);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto2);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto3);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto4);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto5);
            this.cityTemperatureService.addNewTemperatureRecordForCity(recordDto6);
        }

        // then
        CountryWithCitiesDto res = this.cityTemperatureService.getCountryAndCitiesAvgTemperatures();
        assertEquals(1, res.getCitiesAvgList().size());
        assertEquals(0, res.getCitiesAvgList().get(0).getDaytimeAvg());
        assertEquals(17, res.getCitiesAvgList().get(0).getNightAvg());
        assertEquals(17, res.getCitiesAvgList().get(0).getFullDayAvg());
    }
}
